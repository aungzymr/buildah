FROM fedora:30

RUN yum -y update && \
    yum -y install buildah && \
    buildah --version && \
    yum -y clean all

CMD [ "buildah", "--version" ]